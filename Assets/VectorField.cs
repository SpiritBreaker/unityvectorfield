﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct VectorFieldPoint
{
		public Vector3 vector;
		public float magnitude;
		public Vector3 v1;
		public Vector3 v2;

}


public class VectorField : MonoBehaviour {

	public Vector3 direction, endpoint, objpos;
	public Vector3 objmove;
	private float x,z,iks,zet;
	public int 	/*РАЗМЕР МАССИВА*/ size=50; //РАЗМЕР МАССИВА;
	public VectorFieldPoint[,] arr = new VectorFieldPoint[50,50];
	public float scale, distance, minx, minz, direx, direz;

	public Transform control;

	public Transform FieldPosition;
	public Transform FieldScale;


	//на вход подаешь точку, на выходе получаешь ближайший вектор в таком вот формате с его длинной.
	public VectorFieldPoint ClosestPoint(Vector3 inputPoint)
	{

		float min=1000f;
		int min_i=0,min_j=0;

		for (int i=0;i<size;i++)
		{
			for (int j=0;j<size;j++)
			{
				float calculatedDistance = (new Vector3 (inputPoint.x,0,inputPoint.z)-arr[i,j].v1).magnitude;
				if (calculatedDistance < min)
				{
					min = calculatedDistance;
					min_i = i;
					min_j = j;
				}
			}
		}
		VectorFieldPoint point=new VectorFieldPoint();
		point.vector=new Vector3(arr[min_i, min_j].v2.x- arr[min_i, min_j].v1.x,0,arr[min_i, min_j].v2.z- arr[min_i, min_j].v1.z);
		point.magnitude=5.9f-Vector3.Distance(arr[min_i,min_j].v2,arr[min_i,min_j].v1);
		return point;
	}
	
	void Update () 
	{
		 // Условный \ Визуальный (0.0) нашего графика	
		scale=5f; //В scale раз уменьшает масштаб массива
		x=(((-size)+1f)/scale)/2f+FieldPosition.position.x;
		for (int i=0;i<size;i++)
		{
			z=(((-size)+1f)/scale)/2f+FieldPosition.position.z;
			for (int j=0;j<size;j++)
			{
				iks = x;
				zet = z;
				arr[i,j].v1 = new Vector3 (x,0,z); // начальная точка записываемая в массив
				endpoint = new Vector3 (Abscissa(iks,zet,control.position),0,Ordinate(iks,zet,control.position)); // temp конечная точка
				direction = new Vector3 (endpoint.x - arr[i,j].v1.x,0,endpoint.z - arr[i,j].v1.z); // temp направление 
				float distance =5.9f-Vector3.Distance(endpoint,arr[i,j].v1); // temp дистанция
				direction.Normalize(); // нормализация
				arr[i,j].v2 = arr[i,j].v1 + (direction*(distance/10f)); // заносим в массив конечную точку равную == начальная точка + (Направление* (дистанцию/10))
				Debug.DrawLine(arr[i,j].v1,arr[i,j].v2,Color.red,0.001f); //рисуем
				z=z+1f/scale;
			}
			x=x+1f/scale;
	}
	}
		float Abscissa(float xt, float zt, Vector3 dot)
	{
		//return -zt+dot.z; // Blackhole
		return ((2f*xt)-dot.x-FieldPosition.position.x)*FieldScale.localScale.x; // Away
	}

	float Ordinate(float xt, float zt, Vector3 dot)
	{
		//return xt-dot.x; // Blackhole
		return ((2f*zt)-dot.z-FieldPosition.position.z)*FieldScale.localScale.z; // Away
	}
}
