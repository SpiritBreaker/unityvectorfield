﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public struct VectorArr
{
 	public Vector3 v1;
	public Vector3 v2;
}
public class VectorFieldsOld : MonoBehaviour {
	public Vector3 direction, endpoint, dotpos, objpos;
	public Vector3 objmove;
	private float x,z,iks,zet;
	public int size;
	public float scale, distance, minx, minz, direx, direz;
	public GameObject[] actors=new GameObject[10];

	void Start () {

	}

	float Abscissa(float xt, float zt, Vector3 dot)
	{
		//return -zt+dot.z; // Blackhole
		//return (xt-dot.x)/(Mathf.Pow(Mathf.Pow(Mathf.Pow(xt-dot.x,2f)+Mathf.Pow(zt-dot.z,2f),1f/2f),3f)); // Bug
		return (2f*xt)-dot.x; // Away
		//return ((((xt-dot.x+1f)/(Mathf.Pow((xt-dot.x+1f),2f)+Mathf.Pow(zt-dot.y,2f))) + ((xt-dot.x-1f)/(Mathf.Pow((xt-dot.x-1f),2f)+Mathf.Pow(zt-dot.z,2f))))); // Bug
	}

	float Ordinate(float xt, float zt, Vector3 dot)
	{
		//return xt-dot.x; // Blackhole
		//return (zt-dot.z)/(Mathf.Pow(Mathf.Pow(Mathf.Pow(xt-dot.x,2f)+Mathf.Pow(zt-dot.z,2f),1f/2f),3f)); // Bug
		return (2f*zt)-dot.z; // Away
		//return ((((zt-dot.z)/(Mathf.Pow((xt-dot.x+1f),2f)+Mathf.Pow(zt-dot.z,2f))) + ((zt-dot.z)/(Mathf.Pow((xt-dot.x-1f),2f)+Mathf.Pow(zt-dot.z,2f))))); // Bug
	}

	void Update()
	{

		dotpos= GameObject.Find("Dot").transform.position; // Условный \ Визуальный (0.0) нашего графика

		/*РАЗМЕР МАССИВА*/ size=50; //РАЗМЕР МАССИВА
		/*МАСШТАБ МАССИВА*/ scale=5f; //В scale раз уменьшает масштаб массива
		VectorArr[,] arr = new VectorArr[size,size];
		x=(((-size)+1f)/scale)/2f;
		for (int i=0;i<size;i++)
		{
			z=(((-size)+1f)/scale)/2f;
			for (int j=0;j<size;j++)
			{
				iks = x;
				zet = z;
				arr[i,j].v1 = new Vector3 (x,0,z); // начальная точка записываемая в массив
				endpoint = new Vector3 (Abscissa(iks,zet,dotpos),0,Ordinate(iks,zet,dotpos)); // temp конечная точка
				direction = new Vector3 (endpoint.x - arr[i,j].v1.x,0,endpoint.z - arr[i,j].v1.z); // temp направление 
				float distance =5.9f-Vector3.Distance(endpoint,arr[i,j].v1); // temp дистанция
				direction.Normalize(); // нормализация
				arr[i,j].v2 = arr[i,j].v1 + (direction*(distance/10f)); // заносим в массив конечную точку равную == начальная точка + (Направление* (дистанцию/10))
				Debug.DrawLine(arr[i,j].v1,arr[i,j].v2,Color.red,0.001f); //рисуем
				z=z+1f/scale;
			}
			x=x+1f/scale;
		}


		objpos = GameObject.Find("Sphere").transform.position; // Объект под действием поля


		float min=1000f;
		int min_i=0,min_j=0;

		for (int i=0;i<size;i++)
		{
			for (int j=0;j<size;j++)
			{
				float calculatedDistance =( new Vector3 (objpos.x,0,objpos.z)-arr[i,j].v1).magnitude;
				if (calculatedDistance < min)
				{
					min = calculatedDistance;
					min_i = i;
					min_j = j;
				}
			}
		}

		objmove = new Vector3(arr[min_i, min_j].v2.x- arr[min_i, min_j].v1.x,0,arr[min_i, min_j].v2.z- arr[min_i, min_j].v1.z);
		distance =5.9f-Vector3.Distance(arr[min_i, min_j].v2,arr[min_i, min_j].v1);
		GameObject.Find("Sphere").transform.Translate(objmove*Time.deltaTime*(distance/2f));
		//GameObject.Find("Sphere").GetComponent<Rigidbody>().AddForce(objmove*(distance*10f),ForceMode.Force);
		
	}
	}
