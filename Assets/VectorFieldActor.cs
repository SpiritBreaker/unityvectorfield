﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorFieldActor : MonoBehaviour {

	public GameObject VectorFieldObject;
	public float speed;

	void Start()
	{
		speed = Random.Range(1,5);
	}

	void Update()
	{
		VectorFieldPoint point = VectorFieldObject.GetComponent<VectorField>().ClosestPoint(this.gameObject.transform.position);
		//this.gameObject.transform.Translate(point.vector * Time.deltaTime*(point.magnitude*(speed/10f)));
		this.gameObject.GetComponent<Rigidbody>().AddForce(point.vector *(point.magnitude*(speed)),ForceMode.Force);
	}

}
